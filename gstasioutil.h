/*
 * Copyright (C) 2018 Centricular Ltd.
 *   Author: Nirbheek Chauhan <nirbheek@centricular.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __GST_ASIO_UTIL_H__
#define __GST_ASIO_UTIL_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "asio.h"
#include <gst/gst.h>
#include <gst/audio/audio.h>
#include <gst/audio/gstaudiosrc.h>
#include <gst/audio/gstaudiosink.h>

/* Static Caps shared between source, sink, and device provider */
#define GST_ASIO_STATIC_CAPS "audio/x-raw, " \
        "format = (string) " GST_AUDIO_FORMATS_ALL ", " \
        "layout = (string) interleaved, " \
        "rate = " GST_AUDIO_RATE_RANGE ", " \
        "channels = " GST_AUDIO_CHANNELS_RANGE

/* Standard error path */
#define ASE_FAILED_AND(err,func,action) \
  do { \
    if (err != ASE_OK) { \
      GST_ERROR_OBJECT (self, #func " failed (%lx): %s", err, asioerror_to_string (err)); \
      action; \
    } \
  } while (0)

#define ASE_FAILED_RET(err,func,ret) ASE_FAILED_AND(err,func,return ret)

#define ASE_FAILED_GOTO(err,func,where) ASE_FAILED_AND(err,func,res = FALSE; goto where)

gboolean gst_asio_util_allocate_drivers (GstElement * self);
void gst_asio_util_deallocate_drivers (GstElement * self);

gboolean gst_asio_util_get_devices (GstElement * self, gboolean active,
    GList ** devices);

gboolean gst_asio_util_initialize (GstElement * element,
    const gchar * device_name);
void gst_asio_util_deinitialize (GstElement * self);

gboolean gst_asio_util_get_device_format (GstElement * self, gboolean is_input,
    GstCaps * template_caps, guint max_channels, GstCaps ** out_caps);

gboolean gst_asio_util_create_buffers (GstElement * self,
    GstAudioRingBufferSpec * spec, GArray * channels, gboolean is_input,
    glong * out_buffer_size);
void gst_asio_util_dispose_buffers (GstElement * self);

gboolean gst_asio_util_start (GstElement * self);
gint gst_asio_util_src_read (GstElement * self, gpointer data, guint length);
gint gst_asio_util_sink_write (GstElement * self, gpointer data, guint length);
void gst_asio_util_stop (GstElement * self);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* __GST_ASIO_UTIL_H__ */
