/*
 * Copyright (C) 2018 Centricular Ltd.
 *   Author: Nirbheek Chauhan <nirbheek@centricular.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "gstasiosrc.h"
#include "gstasiosink.h"

GST_DEBUG_CATEGORY (gst_asio_debug);

static gboolean
plugin_init (GstPlugin * plugin)
{
  if (!gst_element_register (plugin, "asiosrc", GST_RANK_NONE,
          GST_TYPE_ASIO_SRC))
    return FALSE;

  if (!gst_element_register (plugin, "asiosink", GST_RANK_NONE,
          GST_TYPE_ASIO_SINK))
    return FALSE;

  GST_DEBUG_CATEGORY_INIT (gst_asio_debug, "asio", 0, "Steinberg ASIO plugin");

  return TRUE;
}

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    asio,
    "Steinberg ASIO plugin",
    plugin_init, VERSION, "LGPL", "gstasio", "http://centricular.com")
