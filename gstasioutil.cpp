/*
 * Copyright (C) 2018 Centricular Ltd.
 *   Author: Nirbheek Chauhan <nirbheek@centricular.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <asiodrivers.h>
#include <asio.h>

#include "iasiothiscallresolver.h"
#include "gstasioutil.h"

GST_DEBUG_CATEGORY_EXTERN (gst_asio_debug);
#define GST_CAT_DEFAULT gst_asio_debug

/* Defined by the ASIO SDK in asiodrivers.cpp, and must be initialized by us
 * prior to calling ASIOInit() */
extern AsioDrivers *asioDrivers;

/* Callbacks we will pass to ASIO, which will be called at appropriate times */

/* ASIO v1 buffer switch callback (deprecated) */
static void gst_asio_util_buffer_switch (long index, ASIOBool process_now);
/* ASIO v2 buffer switch callback, this is called when buffers need to be filled */
static ASIOTime * gst_asio_util_buffer_switch_time_info (ASIOTime * time_info,
    long index, ASIOBool process_now);
/* XXX: This is not implemented yet, we assume that the sample rate is not
 * changed once streaming begins */
static void gst_asio_util_sample_rate_changed (ASIOSampleRate rate);
/* Callback for messages from the ASIO driver */
static long gst_asio_util_messages (long selector, long value,
    void * message, double * opt);

/* This struct is passed to ASIOCreateBuffers, and is called for each
 * ASIOBufferInfo structure, which is one structure per channel */
static ASIOCallbacks callbacks = {
  gst_asio_util_buffer_switch,
  gst_asio_util_sample_rate_changed,
  gst_asio_util_messages,
  gst_asio_util_buffer_switch_time_info,
};

/* NOTE: Since the above callbacks do not support user data, we are forced to
 * use global variables for sharing information. */
static guint num_channels;
static GArray *asio_channels;
static ASIOChannelInfo channel_info;
static ASIOBufferInfo *buffer_infos = NULL;

/* We do all writes in the appropriate buffer_switch*() callback, and wait for
 * the write to complete by blocking in the sink_write() method */

/* Signalled when we have data to write out or read */
static GCond buffer_cond;
static GMutex buffer_mutex;
/* We copy the next buffer to be written out here in sink_write(), or
 * we read this as the next buffer in src_read() */
static gpointer buffer_data;
/* Size of each buffer allocated by ASIO (in frames) */
static glong buffer_size;
/* Bytes per sample/frame */
static guint bps;

/* Signalled when we've written out data in buffer_switch() */
static GCond switch_cond;
static GMutex switch_mutex;
/* Bytes written out or read out */
static glong switch_done = 0;

/* Function pointers for AVRT */
typedef HANDLE (WINAPI * AvSetMmThreadCharacteristics) (LPCSTR, LPDWORD);
typedef BOOL (WINAPI * AvRevertMmThreadCharacteristics) (HANDLE);
static struct
{
  HMODULE dll;
  gboolean tried_loading;

  HANDLE (WINAPI * AvSetMmThreadCharacteristics) (LPCSTR, LPDWORD);
  BOOL (WINAPI * AvRevertMmThreadCharacteristics) (HANDLE);
} gst_asio_avrt_tbl = {0, 0, 0, 0};
static HANDLE thread_priority_handle = NULL;

static gboolean
gst_asio_util_init_thread_priority (void)
{
  if (gst_asio_avrt_tbl.tried_loading)
    return gst_asio_avrt_tbl.dll != NULL;

  if (!gst_asio_avrt_tbl.dll)
    gst_asio_avrt_tbl.dll = LoadLibrary (TEXT ("avrt.dll"));

  if (!gst_asio_avrt_tbl.dll) {
    GST_WARNING ("Failed to set thread priority, can't find avrt.dll");
    gst_asio_avrt_tbl.tried_loading = TRUE;
    return FALSE;
  }

  gst_asio_avrt_tbl.AvSetMmThreadCharacteristics =
      reinterpret_cast<AvSetMmThreadCharacteristics>(
          GetProcAddress (gst_asio_avrt_tbl.dll,
          "AvSetMmThreadCharacteristicsA"));
  gst_asio_avrt_tbl.AvRevertMmThreadCharacteristics =
      reinterpret_cast<AvRevertMmThreadCharacteristics>(
          GetProcAddress (gst_asio_avrt_tbl.dll,
          "AvRevertMmThreadCharacteristics"));

  gst_asio_avrt_tbl.tried_loading = TRUE;

  return TRUE;
}

static void
gst_asio_util_set_thread_characteristics (void)
{
  DWORD taskIndex = 0;

  if (!gst_asio_util_init_thread_priority ())
    return;

  thread_priority_handle =
      gst_asio_avrt_tbl.AvSetMmThreadCharacteristics (TEXT ("Pro Audio"),
      &taskIndex);
}

static void
gst_asio_util_revert_thread_characteristics (void)
{
  if (!thread_priority_handle)
    return;

  gst_asio_avrt_tbl.AvRevertMmThreadCharacteristics (thread_priority_handle);
}

const gchar *
asioerror_to_string (ASIOError err)
{
  const gchar *s = "unknown error";

  switch (err) {
    case ASE_NotPresent:
      s = "hardware input or output is not present or available";
      break;
    case ASE_HWMalfunction:
      s = "hardware is malfunctioning";
      break;
    case ASE_InvalidParameter:
      s = "input parameter invalid";
      break;
    case ASE_InvalidMode:
      s = "hardware is in a bad mode or used in a bad mode";
      break;
    case ASE_SPNotAdvancing:
      s = "hardware is not running when sample position is inquired";
      break;
    case ASE_NoClock:
      s = "sample clock or rate cannot be determined or is not present";
      break;
    case ASE_NoMemory:
      s = "not enough memory for completing the request";
      break;
  }

  return s;
}

gboolean
gst_asio_util_allocate_drivers (GstElement * self)
{
  if (asioDrivers) {
    GST_DEBUG_OBJECT (self, "already allocated %li drivers",
        asioDrivers->asioGetNumDev ());
    return TRUE;
  }

  /* The constructor calls CoInitialize() and the deconstructor destroys it */
  asioDrivers = new AsioDrivers ();
  if (!asioDrivers) {
    GST_ERROR_OBJECT (self, "Failed to allocate drivers");
    return FALSE;
  }

  GST_INFO_OBJECT (self, "allocated %li drivers", asioDrivers->asioGetNumDev ());
  return TRUE;
}

void
gst_asio_util_deallocate_drivers (GstElement * self)
{
  delete asioDrivers;
  asioDrivers = NULL;
  GST_INFO_OBJECT (self, "de-allocated drivers");
}

static gboolean
gst_asio_util_get_driver_names (GstElement * self, char *** out_driver_names,
    guint * out_driver_count)
{
  guint ii, driver_count;
  char **driver_names;

  *out_driver_names = NULL;
  *out_driver_count = 0;

  if (!gst_asio_util_allocate_drivers (self))
    return FALSE;

  driver_count = asioDrivers->asioGetNumDev ();
  if (driver_count == 0) {
    GST_ERROR_OBJECT (self, "No drivers found");
    return FALSE;
  }

  GST_INFO_OBJECT (self, "%i driver(s) found\n", driver_count);

  /* char** array of elements at most length 32, null-terminated */
  driver_names = (char **) malloc (driver_count * sizeof (char*));
  for (ii = 0; ii < driver_count; ii++)
    driver_names[ii] = (char *) malloc (32);

  asioDrivers->getDriverNames (driver_names, driver_count);
  for (ii = 0; ii < driver_count; ii++)
    GST_INFO_OBJECT (self, "Found driver: '%.32s'", driver_names[ii]);

  *out_driver_count = driver_count;
  *out_driver_names = driver_names;

  return TRUE;
}

#if 0
gboolean
gst_asio_util_get_devices (GstElement * self, gboolean active,
    GList ** devices)
{
  gboolean res = FALSE;
  static GstStaticCaps scaps = GST_STATIC_CAPS (GST_ASIO_STATIC_CAPS);
  const gchar *device_class, *element_name;
  guint ii, count, driverCount;
  char **driverNames;

  *devices = NULL;

  if (!gst_asio_util_allocate_drivers (self))
    return FALSE;

  driverCount = asioDrivers->asioGetNumDev ();
  if (driverCount == 0) {
    GST_ERROR_OBJECT (self, "No devices found");
    return FALSE;
  }

  GST_INFO_OBJECT (self, "%i devices found\n", driverCount);

  /* char** array of elements at most length 32, null-terminated */
  driverNames = (char **) malloc (driverCount * 32 * sizeof (char*));

  asioDrivers->getDriverNames (driverNames, driverCount);
  for (ii = 0; ii < driverCount; ii++)
    GST_INFO_OBJECT (self, "Found device: '%s'", driverNames[ii]);

  /* Create a GList of GstDevices* to return */
  for (ii = 0; ii < count; ii++) {
    gchar *description, *name;
    GstStructure *props;
    GstDevice *device;
    GstCaps *caps;

    device_class = "Audio/Sink";
    element_name = "asiosink";
    name = driverNames[ii];
    /* FIXME: this should be device-specific */
    caps = gst_static_caps_get (&scaps);

    /* Set some useful properties */
    props = gst_structure_new ("asio-proplist",
        "device.api", G_TYPE_STRING, "asio",
        "device.name", G_TYPE_STRING, GST_STR_NULL (name), NULL);

    device = g_object_new (GST_TYPE_ASIO_DEVICE, "device", name,
        "display-name", name, "caps", caps,
        "device-class", device_class, "properties", props, NULL);
    GST_WASAPI_DEVICE (device)->element = element_name;

    gst_structure_free (props);
    gst_caps_unref (caps);
    *devices = g_list_prepend (*devices, device);

    g_free (description);
    g_free (strid);
  }

  gst_asio_util_deallocate_drivers (self);

  return TRUE;
}
#endif

gboolean
gst_asio_util_initialize (GstElement * self, const gchar * driver_name)
{
  char name[32];
  char **driver_names;
  guint driver_count;
  ASIODriverInfo dinfo;
  ASIOError err;

  if (!gst_asio_util_allocate_drivers (self))
    return FALSE;

  if (!driver_name) {
    if (!gst_asio_util_get_driver_names (self, &driver_names, &driver_count))
      return FALSE;
    driver_name = driver_names[0];
    /* XXX: leaks driver_names */
  }

  GST_INFO_OBJECT (self, "Trying to load driver '%s'", driver_name);

  /* Check if a driver is already loaded */
  if (asioDrivers->getCurrentDriverName (name)) {
    if (strncmp (name, driver_name, 32) != 0) {
      GST_ERROR_OBJECT (self, "Can't load driver '%s', another driver '%s' is "
          "already loaded", driver_name, name);
      return FALSE;
    }
    /* Maybe we messed up while cleaning up? Try to continue. */
    GST_INFO_OBJECT (self, "Driver '%s' is already loaded", name);
  /* Try to load the driver */
  } else if (!asioDrivers->loadDriver ((char* ) driver_name)) {
    GST_ERROR_OBJECT (self, "Failed to load driver '%s'\n", driver_name);
    return FALSE;
  }

  GST_INFO_OBJECT (self, "Loaded '%s' driver, initializing...", driver_name);

  dinfo.asioVersion = 2;
  /* FIXME: leak */
  dinfo.sysRef = GetDesktopWindow ();

  err = ASIOInit (&dinfo);
  ASE_FAILED_RET (err, ASIOInit, FALSE);

  GST_INFO_OBJECT (self, "ASIOInit of driver '%s' succeeded", dinfo.name);

  return TRUE;
}

void
gst_asio_util_deinitialize (GstElement * self)
{
  ASIOExit ();
  gst_asio_util_deallocate_drivers (self);
}

static const gchar *
gst_asio_util_sample_type_to_audio_format (ASIOSampleType type, GstElement * self)
{
  GstAudioFormat fmt;
  const gchar *fmt_str = NULL;

  switch (type) {
    /*~~ MSB means big endian ~~*/
    case ASIOSTInt16MSB:
      fmt = GST_AUDIO_FORMAT_S16BE;
      break;
    /* FIXME: also used for 20 bits packed in 24 bits, how do we detect that? */
    case ASIOSTInt24MSB:
      fmt = GST_AUDIO_FORMAT_S24BE;
      break;
    case ASIOSTInt32MSB:
      fmt = GST_AUDIO_FORMAT_S32BE;
      break;
    case ASIOSTFloat32MSB:
      fmt = GST_AUDIO_FORMAT_F32BE;
      break;
    case ASIOSTFloat64MSB:
      fmt = GST_AUDIO_FORMAT_F64BE;
      break;
    /* All these are aligned to a different boundary than the packing, not sure
     * how to handle it, let's try the normal S32BE format */
    case ASIOSTInt32MSB16:
    case ASIOSTInt32MSB18:
    case ASIOSTInt32MSB20:
    case ASIOSTInt32MSB24:
      fmt = GST_AUDIO_FORMAT_S32BE;
      break;

    /*~~ LSB means little endian ~~*/
    case ASIOSTInt16LSB:
      fmt = GST_AUDIO_FORMAT_S16LE;
      break;
    /* FIXME: also used for 20 bits packed in 24 bits, how do we detect that? */
    case ASIOSTInt24LSB:
      fmt = GST_AUDIO_FORMAT_S24LE;
      break;
    case ASIOSTInt32LSB:
      fmt = GST_AUDIO_FORMAT_S32LE;
      break;
    case ASIOSTFloat32LSB:
      fmt = GST_AUDIO_FORMAT_F32LE;
      break;
    case ASIOSTFloat64LSB:
      fmt = GST_AUDIO_FORMAT_F64LE;
      break;
    /* All these are aligned to a different boundary than the packing, not sure
     * how to handle it, let's try the normal S32LE format */
    case ASIOSTInt32LSB16:
    case ASIOSTInt32LSB18:
    case ASIOSTInt32LSB20:
    case ASIOSTInt32LSB24:
      GST_WARNING_OBJECT (self, "weird alignment %li, trying S32LE", type);
      fmt = GST_AUDIO_FORMAT_S32LE;
      break;

    /*~~ ASIO DSD formats are don't have gstreamer mappings ~~*/
    case ASIOSTDSDInt8LSB1:
    case ASIOSTDSDInt8MSB1:
    case ASIOSTDSDInt8NER8:
      GST_ERROR_OBJECT (self, "ASIO DSD formats are not supported");
      fmt = GST_AUDIO_FORMAT_UNKNOWN;
      break;
    default:
      GST_ERROR_OBJECT (self, "Unknown asio sample type %lx", type);
      fmt = GST_AUDIO_FORMAT_UNKNOWN;
      break;
  }

  if (fmt != GST_AUDIO_FORMAT_UNKNOWN)
    fmt_str = gst_audio_format_to_string (fmt);

  return fmt_str;
}

gboolean
gst_asio_util_get_device_format (GstElement * self, gboolean is_input,
    GstCaps * template_caps, guint max_channels, GstCaps ** out_caps)
{
  ASIOError err;
  ASIOSampleRate sample_rate;
  const gchar *afmt;
  long input_channels, output_channels;
  guint ii;

  err = ASIOGetSampleRate (&sample_rate);
  ASE_FAILED_RET (err, ASIOGetSampleRate, FALSE);

  if (!max_channels) {
    err = ASIOGetChannels (&input_channels, &output_channels);
    ASE_FAILED_RET (err, ASIOGetChannels, FALSE);
    if (is_input)
      max_channels = (guint) input_channels;
    else
      max_channels = (guint) output_channels;
  }
  
  /* We fetch the channel info for just the first channel here to get the
   * audio format being used.
   *
   * XXX: ASIO supports different audio formats for each channel, but it's
   * non-trivial to implement that in gstreamer, so we don't support it. 
   * Other implementations store and maintain separate ASIOChannel structures
   * for each channel (for both input and output), but we don't need to. */
  channel_info.channel = 0;
  channel_info.isInput = is_input;
  err = ASIOGetChannelInfo (&channel_info);
  ASE_FAILED_RET (err, ASIOGetChannelInfo, FALSE);
  GST_INFO ("Fetched channel info for channel '%.32s'", channel_info.name);

  afmt = gst_asio_util_sample_type_to_audio_format (channel_info.type, self);
  if (!afmt)
    return FALSE;

  GST_INFO ("Audio format for '%.32s' is '%s'", channel_info.name, afmt);

  *out_caps = gst_caps_copy (template_caps);

  for (ii = 0; ii < gst_caps_get_size (*out_caps); ii++) {
    GstStructure *s = gst_caps_get_structure (*out_caps, ii);

    gst_structure_set (s,
        "format", G_TYPE_STRING, afmt,
        "channels", GST_TYPE_INT_RANGE, 1, max_channels,
        "rate", G_TYPE_INT, (int) sample_rate,
        /* ASIO doesn't care about the channel mask, so don't return anything */
        NULL);
  }

  return TRUE;
}

static inline guint
gst_channel_index_to_asio_channel_index (guint index)
{
  if (!asio_channels)
    return index;
  return g_array_index (asio_channels, guint, index);
}

gboolean
gst_asio_util_create_buffers (GstElement * self, GstAudioRingBufferSpec * spec,
    GArray * channels, gboolean is_input, glong * out_buffer_size)
{
  ASIOError err;
  glong min_size, max_size, default_size, step;
  guint ii;

  err = ASIOGetBufferSize (&min_size, &max_size, &default_size, &step);
  ASE_FAILED_RET (err, ASIOGetBufferSize, FALSE);

  GST_INFO_OBJECT (self, "buffer size (frames) min: %li, max: %li, preferred: "
      "%li, step: %li", min_size, max_size, default_size, step);

  /* The preferred size is controlled by the control panel provided by the
   * selected ASIO driver, and is the usual way that people select the buffer
   * size. FIXME: This can change at runtime, so we should handle that */
  buffer_size = default_size;

  /* Store channel data for use in the buffer_switch callback */
  num_channels = GST_AUDIO_INFO_CHANNELS (&spec->info);
  if (channels && channels->len > 0)
    asio_channels = g_array_ref (channels);

  /* width here is the bits per sample */
  bps = GST_AUDIO_INFO_WIDTH (&spec->info) >> 3;

  /* Each channel is allocated its own pair of buffers inside the buffer info */
  buffer_infos = g_new0 (ASIOBufferInfo, num_channels);

  for (ii = 0; ii < num_channels; ii++) {
    /* TODO: create buffers for both input and output when both asiosrc and
     * asiosink are used together */
    buffer_infos[ii].isInput = is_input;
    buffer_infos[ii].channelNum = gst_channel_index_to_asio_channel_index (ii);
  }

  err = ASIOCreateBuffers (buffer_infos, num_channels, buffer_size,
      &callbacks);
  ASE_FAILED_RET (err, ASIOCreateBuffers, FALSE);

  for (ii = 0; ii < num_channels; ii++) {
    GST_INFO_OBJECT (self, "channel index %li, buffers %p, %p, size %li frames,"
        " bps %u", buffer_infos[ii].channelNum, buffer_infos[ii].buffers[0],
        buffer_infos[ii].buffers[1], buffer_size, bps);
  }

  *out_buffer_size = buffer_size * num_channels * bps;

  return TRUE;
}

void
gst_asio_util_dispose_buffers (GstElement * self)
{
  GST_OBJECT_LOCK (self);
  g_mutex_lock (&buffer_mutex);
  if (buffer_infos)
    ASIODisposeBuffers ();

  /* Clear global variables initialized in create_buffers() */
  buffer_infos = NULL;
  buffer_data = NULL;
  asio_channels = NULL;
  num_channels = 0;
  bps = 0;
  g_mutex_unlock (&buffer_mutex);
  GST_OBJECT_UNLOCK (self);
}

gboolean
gst_asio_util_start (GstElement * self)
{
  ASIOError err;

  /* Tell the driver to start calling our callbacks */
  err = ASIOStart ();
  ASE_FAILED_RET (err, ASIOStart, FALSE);

  /* Increase the thread priority to reduce glitches */
  gst_asio_util_set_thread_characteristics ();

  return TRUE;
}

gint
gst_asio_util_src_read (GstElement * self, gpointer data, guint length)
{
  glong read;

  /* Assert that the latency_time is what we expect */
  g_assert_cmpint (length, ==, num_channels * buffer_size * bps);

  GST_DEBUG_OBJECT (self, "trying to read %i bytes", length);

  g_mutex_lock (&buffer_mutex);
  /* Signal buffer_switch() that we want to read data into this */
  buffer_data = data;
  g_cond_signal (&buffer_cond);
  g_mutex_unlock (&buffer_mutex);

  g_mutex_lock (&switch_mutex);
  /* Wait for buffer_switch() to finish reading */
  while (switch_done == 0)
    g_cond_wait (&switch_cond, &switch_mutex);
  read = switch_done;
  switch_done = 0;
  g_mutex_unlock (&switch_mutex);

  if (G_UNLIKELY (read != (glong) length))
    GST_WARNING_OBJECT (self, "expected to read %u but read %li", length, read);

  return read;
}

gint
gst_asio_util_sink_write (GstElement * self, gpointer data, guint length)
{
  glong written;

  /* Assert that the latency_time is what we expect */
  g_assert_cmpint (length, ==, num_channels * buffer_size * bps);

  GST_DEBUG_OBJECT (self, "writing %i bytes", length);

  g_mutex_lock (&buffer_mutex);
  /* Signal buffer_switch() that we have data to write out */
  buffer_data = data;
  g_cond_signal (&buffer_cond);
  g_mutex_unlock (&buffer_mutex);

  g_mutex_lock (&switch_mutex);
  /* Wait for buffer_switch() to finish writing */
  while (switch_done == 0)
    g_cond_wait (&switch_cond, &switch_mutex);
  written = switch_done;
  switch_done = 0;
  g_mutex_unlock (&switch_mutex);

  if (G_UNLIKELY (written != (glong) length))
    GST_WARNING_OBJECT (self, "expected to write %u but wrote %li", length,
        written);

  return written;
}

void
gst_asio_util_stop (GstElement * self)
{
  ASIOStop ();

  gst_asio_util_dispose_buffers (self);
  if (asio_channels)
    g_array_unref (asio_channels);

  gst_asio_util_revert_thread_characteristics ();
}

static long
gst_asio_util_messages (long selector, long value, void * message G_GNUC_UNUSED,
    double * opt G_GNUC_UNUSED)
{
  GST_DEBUG ("ASIO message: %li, %li", selector, value);

  switch(selector) {
    case kAsioSelectorSupported:
      if (value == kAsioResetRequest || value == kAsioEngineVersion ||
          value == kAsioResyncRequest || value == kAsioLatenciesChanged ||
          value == kAsioSupportsTimeCode || value == kAsioSupportsInputMonitor)
        return 0;
      else if (value == kAsioSupportsTimeInfo)
        return 1;
      GST_ERROR ("Unsupported ASIO selector: %li", value);
      break;
    case kAsioBufferSizeChange:
      GST_ERROR ("Unsupported ASIO message: kAsioBufferSizeChange");
      break;
    case kAsioResetRequest:
      GST_ERROR ("Unsupported ASIO message: kAsioResetRequest");
      break;
    case kAsioResyncRequest:
      GST_ERROR ("Unsupported ASIO message: kAsioResyncRequest");
      break;
    case kAsioLatenciesChanged:
      GST_WARNING ("Unsupported ASIO message: kAsioLatenciesChanged");
      break;
    case kAsioEngineVersion:
      /* We target the ASIO v2 API, which includes ASIOOutputReady() */
      return 2;
    case kAsioSupportsTimeInfo:
      /* We use the new time info buffer switch callback */
      return 1;
    case kAsioSupportsTimeCode:
      /* We don't use the time code info right now */
      return 0;
    default:
      GST_WARNING ("Unsupported ASIO message: %li, %li", selector, value);
      break;
  }
  return 0;
}

static void
gst_asio_util_sample_rate_changed (ASIOSampleRate rate)
{
  GST_ERROR ("Unhandled new sample rate '%.2f' from ASIO driver", rate);
}

static void
gst_asio_util_buffer_switch (long index, ASIOBool process_now)
{
  gst_asio_util_buffer_switch_time_info (NULL, index, process_now);
}

static ASIOTime *
gst_asio_util_buffer_switch_time_info (ASIOTime * time_info, long index,
    ASIOBool process_now G_GNUC_UNUSED)
{
  gpointer data;
  guint ii, length, gst_offset, asio_offset;

  GST_DEBUG ("buffer switch to %li", index);

  g_mutex_lock (&buffer_mutex);
  /* Wait for data to write from sink_write() or read from src_read() */
  while (!buffer_data)
    g_cond_wait (&buffer_cond, &buffer_mutex);
  data = buffer_data;
  buffer_data = NULL;
  g_mutex_unlock (&buffer_mutex);

  g_mutex_lock (&switch_mutex);
  gst_offset = 0; asio_offset = 0;
  length = num_channels * buffer_size * bps;
  while (gst_offset < length) {
    /* The audio we have is always interleaved and must be de-interleaved
     * before writing out (or interleaved after reading).
     * While writing: copy samples for each channel into/from the correct
     * channel-specific ASIO buffer.
     * While reading: copy samples from each ASIO buffer into the gst buffer
     * in a way that leaves it interleaved. */
    for (ii = 0; ii < num_channels; ii++) {
      if (buffer_infos[ii].isInput)
        memcpy (((char *) data) + gst_offset,
            ((char *) buffer_infos[ii].buffers[index]) + asio_offset, bps);
      else
        memcpy (((char *) buffer_infos[ii].buffers[index]) + asio_offset,
            ((char *) data) + gst_offset, bps);
      gst_offset += bps;
    }
    asio_offset += bps;
  }

  /* Calling this reduces latency because the driver can render this buffer
   * immediately instead of waiting for the next buffer */
  ASIOOutputReady ();

  /* Signal sink_write()/src_read() that we're done */
  switch_done = gst_offset;
  g_cond_signal (&switch_cond);
  g_mutex_unlock (&switch_mutex);

  GST_DEBUG ("read/wrote %i bytes", gst_offset);

  return time_info;
}
