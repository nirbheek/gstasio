/*
 * Copyright (C) 2018 Centricular Ltd.
 *   Author: Nirbheek Chauhan <nirbheek@centricular.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/**
 * SECTION:element-asiosink
 * @title: asiosink
 *
 * Provides audio playback using the proprietary Steinberg ASIO API for Windows
 *
 * ## Example pipelines
 * |[
 * gst-launch-1.0 -v audiotestsrc samplesperbuffer=160 ! asiosink
 * ]| Generate 20 ms buffers and render to the default audio device.
 *
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "gstasiosink.h"

GST_DEBUG_CATEGORY_STATIC (gst_asio_sink_debug);
#define GST_CAT_DEFAULT gst_asio_sink_debug

static GstStaticPadTemplate sink_template = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_ASIO_STATIC_CAPS));

enum
{
  PROP_0,
  PROP_DEVICE,
};

static void gst_asio_sink_dispose (GObject * object);
static void gst_asio_sink_finalize (GObject * object);
static void gst_asio_sink_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_asio_sink_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);
static GstCaps *gst_asio_sink_get_caps (GstBaseSink * bsink,
    GstCaps * filter);

static gboolean gst_asio_sink_prepare (GstAudioSink * asink,
    GstAudioRingBufferSpec * spec);
static gboolean gst_asio_sink_unprepare (GstAudioSink * asink);
static gboolean gst_asio_sink_open (GstAudioSink * asink);
static gboolean gst_asio_sink_close (GstAudioSink * asink);
static gint gst_asio_sink_write (GstAudioSink * asink,
    gpointer data, guint length);
static guint gst_asio_sink_delay (GstAudioSink * asink);
static void gst_asio_sink_reset (GstAudioSink * asink);

#define gst_asio_sink_parent_class parent_class
G_DEFINE_TYPE (GstAsioSink, gst_asio_sink, GST_TYPE_AUDIO_SINK);

static void
gst_asio_sink_class_init (GstAsioSinkClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstElementClass *gstelement_class = GST_ELEMENT_CLASS (klass);
  GstBaseSinkClass *gstbasesink_class = GST_BASE_SINK_CLASS (klass);
  GstAudioSinkClass *gstaudiosink_class = GST_AUDIO_SINK_CLASS (klass);

  gobject_class->dispose = gst_asio_sink_dispose;
  gobject_class->finalize = gst_asio_sink_finalize;
  gobject_class->set_property = gst_asio_sink_set_property;
  gobject_class->get_property = gst_asio_sink_get_property;

  g_object_class_install_property (gobject_class,
      PROP_DEVICE,
      g_param_spec_string ("device", "Device",
          "ASIO device driver name to use", NULL,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS |
          GST_PARAM_MUTABLE_READY));

  gst_element_class_add_static_pad_template (gstelement_class, &sink_template);
  gst_element_class_set_static_metadata (gstelement_class, "AsioSink",
      "Sink/Audio",
      "Stream audio to an audio device through ASIO",
      "Nirbheek Chauhan <nirbheek@centricular.com>");

  gstbasesink_class->get_caps = GST_DEBUG_FUNCPTR (gst_asio_sink_get_caps);

  gstaudiosink_class->prepare = GST_DEBUG_FUNCPTR (gst_asio_sink_prepare);
  gstaudiosink_class->unprepare = GST_DEBUG_FUNCPTR (gst_asio_sink_unprepare);
  gstaudiosink_class->open = GST_DEBUG_FUNCPTR (gst_asio_sink_open);
  gstaudiosink_class->close = GST_DEBUG_FUNCPTR (gst_asio_sink_close);
  gstaudiosink_class->write = GST_DEBUG_FUNCPTR (gst_asio_sink_write);
  gstaudiosink_class->delay = GST_DEBUG_FUNCPTR (gst_asio_sink_delay);
  gstaudiosink_class->reset = GST_DEBUG_FUNCPTR (gst_asio_sink_reset);

  GST_DEBUG_CATEGORY_INIT (gst_asio_sink_debug, "asiosink", 0,
        "Steinberg ASIO sink");
}

static void
gst_asio_sink_init (GstAsioSink * self)
{
  self->drivers_allocated = FALSE;
  self->driver_loaded = FALSE;
  self->driver_name = NULL;

  if (gst_asio_util_allocate_drivers (GST_ELEMENT (self)))
    self->drivers_allocated = TRUE;
}

static void
gst_asio_sink_dispose (GObject * object)
{
  //GstAsioSink *self = GST_ASIO_SINK (object);

  G_OBJECT_CLASS (gst_asio_sink_parent_class)->dispose (object);
}

static void
gst_asio_sink_finalize (GObject * object)
{
  GstAsioSink *self = GST_ASIO_SINK (object);

  g_clear_pointer (&self->driver_name, g_free);

  if (self->cached_caps != NULL) {
    gst_caps_unref (self->cached_caps);
    self->cached_caps = NULL;
  }

  gst_asio_util_deallocate_drivers (GST_ELEMENT (self));

  G_OBJECT_CLASS (gst_asio_sink_parent_class)->finalize (object);
}

static void
gst_asio_sink_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstAsioSink *self = GST_ASIO_SINK (object);

  switch (prop_id) {
    case PROP_DEVICE:
      g_free (self->driver_name);
      self->driver_name = g_value_dup_string (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_asio_sink_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstAsioSink *self = GST_ASIO_SINK (object);

  switch (prop_id) {
    case PROP_DEVICE:
      g_value_set_string (value, self->driver_name);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static GstCaps *
gst_asio_sink_get_caps (GstBaseSink * bsink, GstCaps * filter)
{
  GstAsioSink *self = GST_ASIO_SINK (bsink);
  GstCaps *caps = NULL;

  GST_DEBUG_OBJECT (self, "entering get caps");

  if (self->cached_caps) {
    caps = gst_caps_ref (self->cached_caps);
  } else {
    GstCaps *template_caps;
    gboolean ret;

    template_caps = gst_pad_get_pad_template_caps (bsink->sinkpad);

    if (!self->driver_loaded) {
      caps = template_caps;
      goto out;
    }

    ret = gst_asio_util_get_device_format (GST_ELEMENT (self), FALSE,
        template_caps, 0, &caps);
    if (!ret) {
      GST_ELEMENT_ERROR (self, STREAM, FORMAT, (NULL),
          ("failed to detect device format"));
      gst_caps_unref (template_caps);
      return NULL;
    }

    gst_caps_replace (&self->cached_caps, caps);
    gst_caps_unref (template_caps);
  }

  if (filter) {
    GstCaps *filtered =
        gst_caps_intersect_full (filter, caps, GST_CAPS_INTERSECT_FIRST);
    gst_caps_unref (caps);
    caps = filtered;
  }

out:
  GST_DEBUG_OBJECT (self, "returning caps %" GST_PTR_FORMAT, caps);
  return caps;
}

static gboolean
gst_asio_sink_open (GstAudioSink * asink)
{
  GstAsioSink *self = GST_ASIO_SINK (asink);

  if (self->driver_name)
    GST_DEBUG_OBJECT (self, "loading driver %s", self->driver_name);
  else
    GST_DEBUG_OBJECT (self, "loading the first driver found");

  if (self->driver_loaded)
    return TRUE;

  if (!gst_asio_util_initialize (GST_ELEMENT (self), self->driver_name)) {
      GST_ELEMENT_ERROR (self, RESOURCE, OPEN_WRITE, (NULL),
          ("Failed to load driver %s", self->driver_name));
    return FALSE;
  }

  self->driver_loaded = TRUE;

  return TRUE;
}

static gboolean
gst_asio_sink_close (GstAudioSink * asink)
{
  GstAsioSink *self = GST_ASIO_SINK (asink);

  gst_asio_util_deinitialize (GST_ELEMENT (self));

  return TRUE;
}

static gboolean
gst_asio_sink_prepare (GstAudioSink * asink, GstAudioRingBufferSpec * spec)
{
  GstAsioSink *self = GST_ASIO_SINK (asink);
  glong buffer_size;
  gboolean res = FALSE;

  if (!gst_asio_util_create_buffers (GST_ELEMENT (self), spec, NULL, FALSE,
      &buffer_size))
    goto beach;

  /* Set our ringbuffer's segsize as ASIO's buffer_size (for all channels
   * combined), and segtotal as 2 for the lowest possible latency. This will be
   * automatically translated to latency_time and buffer_time. */
  spec->segsize = buffer_size;
  spec->segtotal = 2;

  if (!gst_asio_util_start (GST_ELEMENT (self)))
    goto beach;

  res = TRUE;
beach:
  if (!res)
    gst_asio_sink_unprepare (asink);

  return res;
}

static gboolean
gst_asio_sink_unprepare (GstAudioSink * asink)
{
  GstAsioSink *self = GST_ASIO_SINK (asink);

  gst_asio_util_dispose_buffers (GST_ELEMENT (self));

  return TRUE;
}

static gint
gst_asio_sink_write (GstAudioSink * asink, gpointer data, guint length)
{
  GstAsioSink *self = GST_ASIO_SINK (asink);

  return gst_asio_util_sink_write (GST_ELEMENT (self), data, length);
}

static guint
gst_asio_sink_delay (GstAudioSink * asink G_GNUC_UNUSED)
{
  //GstAsioSink *self = GST_ASIO_SINK (asink);

  /* TODO: Implement this with ASIOGetLatencies */
  return 0;
}

static void
gst_asio_sink_reset (GstAudioSink * asink)
{
  GstAsioSink *self = GST_ASIO_SINK (asink);

  gst_asio_util_stop (GST_ELEMENT (self));
  /* TODO: restart on next call to sink_write */
}
